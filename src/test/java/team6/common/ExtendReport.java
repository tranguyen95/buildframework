package team6.common;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExtendReport {
    public static void main(String[] args) {

        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("./output/extent.html");

        ExtentReports extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        ExtentTest test1 = extent.createTest("Test Extent Report", "Team 6");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        test1.log(Status.INFO,"Start Testcase");
        driver.get("https://google.com");
        test1.pass("Navegate to google.com");

        driver.findElement(By.name("q")).sendKeys("abc");
        test1.pass("Enter text in search box");
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]")).click();
        test1.pass("Pressed enter");

        driver.close();
        driver.quit();
        test1.pass("Close browser");
        test1.info("Test completed");

        extent.flush();
    }

}
