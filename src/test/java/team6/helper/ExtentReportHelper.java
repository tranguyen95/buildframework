package team6.helper;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.openqa.selenium.WebDriver;

public class ExtentReportHelper {
    public static WebDriver driver;

    public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test1;


    public static void setupExtentReport() {
        System.out.println("start setup extent");
        htmlReporter = new ExtentHtmlReporter("./output/report.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        test1  = extent.createTest("Test Extent Report", "Team 6");
    }

    public static void endExtentReport() {
        System.out.println("End extent");
        extent.flush();
    }
}
