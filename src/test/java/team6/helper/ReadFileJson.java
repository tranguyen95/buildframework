package team6.helper;

import com.google.gson.Gson;
import org.testng.annotations.Parameters;
import team6.common.User;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadFileJson {


    public ReadFileJson() throws IOException {
    }

    public User readFileUser(String filePath) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(filePath));
        System.out.println(filePath);
        Gson gson = new Gson();
        User[] users = gson.fromJson(reader, User[].class);
        return users[0];
    }
//    public User readFileUser2() {
//        return users[1];
//    }



}
