package team6.helper;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@Getter
@Setter
public class DriverHelper {
    public static WebDriver driver;

    public static WebDriver setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        return driver;
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

}
